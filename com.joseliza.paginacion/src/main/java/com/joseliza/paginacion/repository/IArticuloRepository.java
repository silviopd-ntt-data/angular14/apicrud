package com.joseliza.paginacion.repository;

import com.joseliza.paginacion.model.Articulo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IArticuloRepository extends JpaRepository<Articulo, Long> {
}
