package com.joseliza.paginacion.controller;

import com.joseliza.paginacion.model.Articulo;
import com.joseliza.paginacion.service.ArticuloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("articulo")
@CrossOrigin(origins = "http://localhost:4200")
public class ArticuloController {
    @Autowired
    private ArticuloService articuloService;

    @PostMapping
    public ResponseEntity<Articulo> saveArticulo(@Valid @RequestBody Articulo articulo) {
        return ResponseEntity.status(HttpStatus.CREATED).body(articuloService.saveArticulo(articulo));
    }

    @GetMapping
    public ResponseEntity<Page<Articulo>> getAllStudent(
            @RequestParam(required = false,defaultValue = "0") Integer page,
            @RequestParam(required = false,defaultValue = "10") Integer size,
            @RequestParam(required = false,defaultValue = "false") Boolean enablePagination) {
        return ResponseEntity.ok(articuloService.getAllArticulo(page, size, enablePagination));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteArticulo(@PathVariable ("id") Long id){
        articuloService.deleteArticulo(id);
        return ResponseEntity.ok(!articuloService.existById(id));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Optional<Articulo>> findArticuloById(@PathVariable ("id") Long id){
        return ResponseEntity.status(HttpStatus.OK).body(articuloService.findById(id));
    }

    @PutMapping
    public ResponseEntity<Articulo> editArticulo (@Valid @RequestBody Articulo articulo){
        return ResponseEntity.status(HttpStatus.CREATED).body(articuloService.editArticulo(articulo));
    }
}

