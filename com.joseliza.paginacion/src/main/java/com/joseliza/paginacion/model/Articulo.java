package com.joseliza.paginacion.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@Entity
@Table(name = "Articulo")
public class Articulo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idArticulo;
    @NotBlank
    private String nombre;
    private double precio;
    private double costo;
    private Date fecha;
    private String descripcion;
}
