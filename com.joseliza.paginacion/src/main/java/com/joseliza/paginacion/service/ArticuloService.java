package com.joseliza.paginacion.service;

import com.joseliza.paginacion.model.Articulo;
import com.joseliza.paginacion.repository.IArticuloRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ArticuloService {
    @Autowired
    private IArticuloRepository iArticuloRepository;

    public Articulo saveArticulo (Articulo product){
        if (product.getIdArticulo() == null){
            return iArticuloRepository.save(product);
        }
        return null;
    }

    public Page<Articulo> getAllArticulo (Integer page, Integer size, Boolean enablePagination){
        return iArticuloRepository.findAll(enablePagination ? PageRequest.of(page, size): Pageable.unpaged());
    }

    public Optional<Articulo> findById(Long id){
        return iArticuloRepository.findById(id);
    }

    public void deleteArticulo(Long id){
        iArticuloRepository.deleteById(id);
    }

    public Articulo editArticulo (Articulo product){
        if (product.getIdArticulo() != null && iArticuloRepository.existsById(product.getIdArticulo())){
            return iArticuloRepository.save(product);
        }
        return null;
    }

    public boolean existById(Long id) {
        return iArticuloRepository.existsById(id);
    }
}
