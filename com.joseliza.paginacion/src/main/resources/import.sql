INSERT INTO Producto(nombre, precio, fecha, direccion, ubicacion) VALUES ('laptop ASUS',3000,'2022-10-30','Lima, Perú', 'Lima');
INSERT INTO Producto(nombre, precio, fecha, direccion, ubicacion) VALUES ('laptop Samsung',2000,'2022-10-30','Lima, Perú', 'Lima');
INSERT INTO Producto(nombre, precio, fecha, direccion, ubicacion) VALUES ('laptop Compaq',3500,'2022-10-30','Lima, Perú', 'Lima');
INSERT INTO Producto(nombre, precio, fecha, direccion, ubicacion) VALUES ('laptop Dell',3600,'2022-10-30','Lima, Perú', 'Lima');
INSERT INTO Producto(nombre, precio, fecha, direccion, ubicacion) VALUES ('laptop Toshiba',2300,'2022-10-30','Lima, Perú','Lima');
INSERT INTO Producto(nombre, precio, fecha, direccion, ubicacion) VALUES ('Celular ASUS',3000,'2022-10-30','Lima, Perú', 'Lima');
INSERT INTO Producto(nombre, precio, fecha, direccion, ubicacion) VALUES ('Celular Samsung',2000,'2022-10-30','Lima, Perú', 'Lima');
INSERT INTO Producto(nombre, precio, fecha, direccion, ubicacion) VALUES ('Celular Compaq',3500,'2022-10-30','Lima, Perú', 'Lima');
INSERT INTO Producto(nombre, precio, fecha, direccion, ubicacion) VALUES ('Celular Dell',3600,'2022-10-30','Lima, Perú', 'Lima');
INSERT INTO Producto(nombre, precio, fecha, direccion, ubicacion) VALUES ('Celular Toshiba',2300,'2022-10-30','Lima, Perú','Lima');


INSERT INTO Articulo(nombre, precio,costo, fecha, descripcion) VALUES ('Articulo 1',3000,3000,'2022-10-30','Descripcion Articulo 1');
INSERT INTO Articulo(nombre, precio,costo, fecha, descripcion) VALUES ('Articulo 2',2000,2000,'2022-10-30','Descripcion Articulo 2');
INSERT INTO Articulo(nombre, precio,costo, fecha, descripcion) VALUES ('Articulo 3',3500,3500,'2022-10-30','Descripcion Articulo 3');
INSERT INTO Articulo(nombre, precio,costo, fecha, descripcion) VALUES ('Articulo 4',3600,3600,'2022-10-30','Descripcion Articulo 4');
INSERT INTO Articulo(nombre, precio,costo, fecha, descripcion) VALUES ('Articulo 5',2300,2300,'2022-10-30','Descripcion Articulo 5');
INSERT INTO Articulo(nombre, precio,costo, fecha, descripcion) VALUES ('Articulo 6',3000,3000,'2022-10-30','Descripcion Articulo 6');
INSERT INTO Articulo(nombre, precio,costo, fecha, descripcion) VALUES ('Articulo 7',2000,2000,'2022-10-30','Descripcion Articulo 7');
INSERT INTO Articulo(nombre, precio,costo, fecha, descripcion) VALUES ('Articulo 8',3500,3500,'2022-10-30','Descripcion Articulo 8');
INSERT INTO Articulo(nombre, precio,costo, fecha, descripcion) VALUES ('Articulo 9',3600,3600,'2022-10-30','Descripcion Articulo 9');
INSERT INTO Articulo(nombre, precio,costo, fecha, descripcion) VALUES ('Articulo 10',2300,2300,'2022-10-30','Descripcion Articulo 10');